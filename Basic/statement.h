/*
 * File: statement.h
 * -----------------
 * This file defines the Statement abstract type.  In
 * the finished version, this file will also specify subclasses
 * for each of the statement types.  As you design your own
 * version of this class, you should pay careful attention to
 * the exp.h interface specified in Chapter 17, which is an
 * excellent model for the Statement class hierarchy.
 */

#ifndef _statement_h
#define _statement_h

#include "evalstate.h"
#include "exp.h"
#include "parser.h"
#include "../StanfordCPPLib/tokenscanner.h"
#include "../StanfordCPPLib/simpio.h"

enum StatementType { REM, LET, PRINT, INPUT, END, GOTO, IF };
/*
 * Class: Statement
 * ----------------
 * This class is used to represent a statement in a program.
 * The model for this class is Expression in the exp.h interface.
 * Like Expression, Statement is an abstract class with subclasses
 * for each of the statement and command types required for the
 * BASIC interpreter.
 */

class Statement {

public:

/*
 * Constructor: Statement
 * ----------------------
 * The base class constructor is empty.  Each subclass must provide
 * its own constructor.
 */

   Statement();

/*
 * Destructor: ~Statement
 * Usage: delete stmt;
 * -------------------
 * The destructor deallocates the storage for this expression.
 * It must be declared virtual to ensure that the correct subclass
 * destructor is called when deleting a statement.
 */

   virtual ~Statement();

/*
 * Method: execute
 * Usage: stmt->execute(state);
 * ----------------------------
 * This method executes a BASIC statement.  Each of the subclasses
 * defines its own execute method that implements the necessary
 * operations.  As was true for the expression evaluator, this
 * method takes an EvalState object for looking up variables or
 * controlling the operation of the interpreter.
 */
   virtual void Check() = 0;
   virtual int Execute(EvalState & state) = 0;
   virtual StatementType Type() = 0;
};

/*
 * The remainder of this file must consists of subclass
 * definitions for the individual statement forms.  Each of
 * those subclasses must define a constructor that parses a
 * statement from a scanner and a method called execute,
 * which executes that statement.  If the private data for
 * a subclass includes data allocated on the heap (such as
 * an Expression object), the class implementation must also
 * specify its own destructor method to free that memory.
 */

class REMstatement : public Statement{
private:
   string line;
public:
	REMstatement(string str) : line(str) { return; }
	~REMstatement() { return; }
    void Check() { return; }
	int Execute(EvalState & state) { return -1; };
	StatementType Type() { return REM; };
};

class LETstatement : public Statement{
private:
   string line;
   string var;
   Expression *exp;
public:
   LETstatement(string str) : line(str), var(""), exp(nullptr) {}
   ~LETstatement() { delete exp; }
   void Check();
   int Execute(EvalState & state)
   {
	   state.setValue(var, exp->eval(state));
	   return -1;
   }
   StatementType Type() { return LET; };
};

class PRINTstatement : public Statement{
private:
   string line;
   Expression *exp;
public:
   PRINTstatement(string str) : line(str), exp(nullptr) {}
   ~PRINTstatement() { delete exp; }
   void Check();
   int Execute(EvalState & state)
   {
	   cout << exp->eval(state) << endl;
	   return -1;
   }
   StatementType Type() { return PRINT; }
};

class INPUTstatement : public Statement{
private:
   string line;
   string var;
public:
   INPUTstatement(string str) : line(str), var("") {}
   ~INPUTstatement() { }
   void Check();
   int Execute(EvalState & state);
   StatementType Type() { return INPUT; }
};

class ENDstatement : public Statement{
private:
   string line;
public:
   ENDstatement(string str) : line(str) {}
   ~ENDstatement() {}
   void Check();
   int Execute(EvalState & state) { return -1; }
   StatementType Type() { return END; }
};

class GOTOstatement : public Statement {
private:
	string line;
	int destination;
public:
	GOTOstatement(string str) : line(str), destination(-1) {}
	~GOTOstatement() {}
	void Check();
	int Execute(EvalState & state) { return destination; }
	StatementType Type() { return GOTO; };
};

class IFstatement : public Statement{
private:
   string line;
   string cmp;
   Expression *exp1, *exp2;
   int destination;
public:
   IFstatement(string str) : line(str), cmp(""), exp1(nullptr), exp2(nullptr), destination(-1) {}
   ~IFstatement() { delete exp1; delete exp2; }
   void Check();
   int Execute(EvalState & state);
   StatementType Type() { return IF; }
};

#endif
