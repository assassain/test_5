/*
 * File: Basic.cpp
 * ---------------
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * This file is the starter project for the BASIC interpreter from
 * Assignment #6.
 * [TODO: extend and correct the documentation]
 */

#include <cctype>
#include <iostream>
#include <string>
#include "exp.h"
#include "parser.h"
#include "program.h"
#include "../StanfordCPPLib/error.h"
#include "../StanfordCPPLib/tokenscanner.h"

#include "../StanfordCPPLib/simpio.h"
#include "../StanfordCPPLib/strlib.h"
using namespace std;

/* Function prototypes */
bool preexist(string st)
{
	return (st == "REM") || (st == "LET") || (st == "PRINT") || (st == "INPUT")
		|| (st == "END") || (st == "GOTO") || (st == "IF") || (st == "THEN") ||
		(st == "RUN") || (st == "LIST") || (st == "CLEAR") || (st == "QUIT") || (st == "HELP");
}
// exclude line number
bool individualcheck(string st)
{
	return  (st == "LET") || (st == "PRINT") || (st == "INPUT") || 
            (st == "RUN") || (st == "LIST") || (st == "CLEAR") || 
		    (st == "QUIT") || (st == "HELP");
}
// include line number 
bool combinecheck(string st)
{
	return  (st == "LET") || (st == "PRINT") || (st == "INPUT") ||
		(st == "END") || (st == "IF") || (st == "GOTO") ||
		(st == "REM") ;
}
void processLine(string line, Program & program, EvalState & state);
int main() {
	EvalState state;
	Program program;
	while (true) {
		try {
			processLine(getLine(), program, state);
		}
		catch (ErrorException & ex) {
			cout << ex.getMessage() << endl;
		}
	}
	return 0;
}

/*
 * Function: processLine
 * Usage: processLine(line, program, state);
 * -----------------------------------------
 * Processes a single line entered by the user.  In this version,
 * the implementation does exactly what the interpreter program
 * does in Chapter 19: read a line, parse it as an expression,
 * and then print the result.  In your implementation, you will
 * need to replace this method with one that can respond correctly
 * when the user enters a program line (which begins with a number)
 * or one of the BASIC commands, such as LIST or RUN.
 */

void processLine(string line, Program & program, EvalState & state) {
	TokenScanner scanner;
	scanner.ignoreWhitespace();
	scanner.scanNumbers();
	scanner.setInput(line);

	string st = scanner.nextToken();
	if (scanner.getTokenType(st) != NUMBER && !preexist(st))
		error("SYNTAX ERROR");

	if (preexist(st))
	{
		Statement *statement = nullptr;
		try {
			int flag = 0;
			if (st == "LET")
			  {
				flag = 1; 
				statement = new LETstatement(line);
				statement->Check();
				int tmp = statement->Execute(state);
		  	  }
			if (st == "PRINT")
			  {
				flag = 1;
				statement = new PRINTstatement(line);
				statement->Check();
				int tmp = statement->Execute(state);
		      }
			if (st == "INPUT") 
		      {
				flag = 1;
				statement = new INPUTstatement(line);
				statement->Check();
				int tmp = statement->Execute(state);
		       } 
			if (flag == 0 && scanner.hasMoreTokens()) error("SYNTAX ERROR");
			if (st == "RUN")  program.RUN(state);
		    if (st == "LIST") program.showlist();
			if (st == "CLEAR") 
			  {
			    program.clear();
				state.Clear();
		 	  }
			if (st == "QUIT") exit(0);
			if (st == "HELP") cout << "MY NAME IS LI LUO XUAN" << endl;
			if (!individualcheck(st)) error("SYNTAX ERROR");
		}
		  catch ( ErrorException & ex){
		  delete statement;
		  error(ex.getMessage());
		}
	    delete statement;
   }
	else {
		Statement *statement = nullptr;
		int lineNumber= atoi(st.data());
		if (!scanner.hasMoreTokens()) 
			program.removeSourceLine(lineNumber);
		else {
		  try {
			  
			string str = scanner.nextToken();
			if (str == "REM")
			{
				
				statement = new REMstatement(line);

			}
			 if (str == "LET")
			   {
			     statement = new LETstatement(line);
				 statement->Check();
			   }
			if (str == "PRINT")
			  {
				statement = new PRINTstatement(line);
				statement->Check();
			  }
			if (str == "INPUT")
			  {
				statement = new INPUTstatement(line);
				statement->Check();
			  }
		    if (str == "END")
			  {
				statement = new ENDstatement(line);
				statement->Check();
			  }
		    if (str == "GOTO")
			  {
				statement = new GOTOstatement(line);
				statement->Check();
			   }
			if (str == "IF")
			  {
				statement = new IFstatement(line);
				statement->Check();
			  }
			if (!combinecheck(str)) error("SYNTAX ERROR");
			}
			catch (ErrorException & ex) {
				delete statement;
				cout << "wyq" << endl;
				error(ex.getMessage());
			}
			
			program.addSourceLine(lineNumber, line);
			program.setParsedStatement(lineNumber, statement);
		}
	}
   /*Expression *exp = parseExp(scanner);
   int value = exp->eval(state);
   cout << value << endl;
   delete exp; */
}
